# README

The research presented in the Jupyter notebook `notebook.ipynb` was accepted at the 2020 IEEE Big Data Conference and can be found [here](https://doi.org/10.1109/BigData50022.2020.9378084). The arXiv version of the manuscript can be found [at this page](https://arxiv.org/abs/2010.10352). The notebook available in this repository was written and executed via the Google Colaboratory interface which can be found at the following location:

https://colab.research.google.com/drive/1mFHvBQjRdj3D7gsFSU7itfoZ8ECb5Uml

Please use the following citation for future reference to this work:

`V. Dumont, V. R. Tribaldos, J. Ajo-Franklin and K. Wu, "Deep Learning for Surface Wave Identification in Distributed Acoustic Sensing Data," 2020 IEEE International Conference on Big Data (Big Data), 2020, pp. 1293-1300, doi: 10.1109/BigData50022.2020.9378084.`

Thank you,<br>
Vincent Dumont<br>
vincentdumont11@gmail.com
